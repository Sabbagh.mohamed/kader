module.exports = {
  up: (queryInterface, DataTypes) => {
    return queryInterface.createTable('invoice', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.BIGINT.UNSIGNED,
      },
      status_id: {
        type: DataTypes.BIGINT.UNSIGNED,
        allowNull: false,
        references: {
          model: 'invoice_status',
          key: 'id',
        },
      },
      note: {
        type: DataTypes.STRING(2000),
        allowNull: false,
      },
      invoice_to: {
        type: DataTypes.STRING(100),
        allowNull: false,
      },
      obsoleted: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      created_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.literal('NOW()'),
        allowNull: false,
      },
      updated_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.literal('NOW()'),
        allowNull: false,
      },
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('invoice');
  },
};
