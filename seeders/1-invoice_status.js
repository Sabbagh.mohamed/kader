'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    return queryInterface.bulkInsert('invoice_status', [
      {name: 'open'},
      {name: 'paid'},
      {name: 'outstanding'},
      {name: 'late'},
    ], {});
  },

  async down(queryInterface, Sequelize) {
    return queryInterface.bulkDelete('invoice_status', null, {});
  },
};
