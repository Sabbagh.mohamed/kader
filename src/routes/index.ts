import express from 'express';
import Success from '../framework/response/success/successes/Success';
import logger from '../framework/logger';
import service from '../service/healthCheck';
import InternalServerError
  from '../framework/response/error/errors/InternalServerError';
import ExpressReq from '../types/expressRequest';
import invoice from './invoice';

const router = express.Router();

router.get('/health-check', async (req: ExpressReq, res, next) => {

  try {
    const {code, message} = await service.dbHealthCheck(req);

    if (code === 0) {
      return next(new Success(message));
    }
  } catch (err) {
    logger.error(`${req.reqUUID} ${err.stack}`);
    return next(new InternalServerError());
  }
});

router.use('/invoice', invoice)

export default router;
