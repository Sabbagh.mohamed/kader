import express from 'express';
import Success from '../../framework/response/success/successes/Success';
import validateRequest from '../../framework/validation';
import logger from '../../framework/logger';
import service from '../../service/invoice';
import InternalServerError
  from '../../framework/response/error/errors/InternalServerError';
import ExpressReq from '../../types/expressRequest';
import validationSchema from '../../validationSchema/invoice';
import NoContent from '../../framework/response/success/successes/NoContent';
import BadRequest from '../../framework/response/error/errors/BadRequest';

const router = express.Router();

router.post('/', validationSchema.createInvoice, validateRequest, async (req: ExpressReq, res: express.Response, next: express.NextFunction) => {
  try {
    const {code, message, data} = await service.createInvoice(req);

    if (code === 0) {
      return next(new Success(message, data));
    }
  } catch (err) {
    logger.error(`${req.reqUUID} ${err.stack}`);
    return next(new InternalServerError());
  }
});

router.get('/', validationSchema.listInvoice, validateRequest, async (req: ExpressReq, res: express.Response, next: express.NextFunction) => {
  try {
    const {code, message, data} = await service.listInvoice(req);

    if (code === 0) {
      return next(new Success(undefined, data));
    } else {
      return next(new NoContent(message, data));
    }
  } catch (err) {
    logger.error(`${req.reqUUID} ${err.stack}`);
    return next(new InternalServerError());
  }
});

router.put('/:id', validationSchema.updateInvoiceStatus, validateRequest, async (req: ExpressReq, res: express.Response, next: express.NextFunction) => {
  try {
    const {code, message} = await service.updateInvoiceStatus(req);

    if (code === 0) {
      return next(new Success(message));
    } else {
      return next(new BadRequest(message));
    }
  } catch (err) {
    logger.error(`${req.reqUUID} ${err.stack}`);
    return next(new InternalServerError());
  }
});

export default router;
