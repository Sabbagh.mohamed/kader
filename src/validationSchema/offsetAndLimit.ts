import {query} from 'express-validator';

const offset = query('offset').isInt({min: 0}).withMessage(`offset query is not a valid number and must me < 0`).optional().toInt();
const limit = query('limit').isInt({
  min: 1,
  max: 100,
}).withMessage(`limit query is not a valid number and must me > 1 and < 100`).optional().toInt();

export default {
  offset,
  limit,
};
