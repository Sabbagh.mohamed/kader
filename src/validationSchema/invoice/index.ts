import {body, query, param} from 'express-validator';
import offsetAndLimit from '../offsetAndLimit';

const invoiceStatusList = ['paid', 'outstanding', 'late', 'open'];

const createInvoice = [
  body('items').exists().withMessage('items is required').isArray({min: 1}).withMessage('items length must be <= 1'),
  body('items.*.description').exists().withMessage('items.*.description is required').isString().withMessage('items.*.description must be string').trim().isLength({
    max: 2000,
    min: 1,
  }).withMessage('items.*.description must be > 1 < 2000'),
  body('items.*.hours').exists().withMessage('items.*.hours is required').isFloat({min: 1}).withMessage('items.*.hours is not valid number and <=1'),
  body('items.*.rate').exists().withMessage('items.*.rate is required').isFloat({min: 1}).withMessage('items.*.rate is not valid number and <=1'),
  body('items.*.expenses').exists().withMessage('items.*.expenses is required').isFloat({min: 1}).withMessage('items.*.expenses is not valid number and <=1'),
  body('to').exists().withMessage('to is required').isEmail().withMessage('to is not valid email').trim().isLength({
    max: 100,
    min: 1,
  }).withMessage('to must be > 1 < 100'),
  body('note').exists().withMessage('note is required').isString().withMessage('note must be string').trim().isLength({
    max: 2000,
    min: 1,
  }).withMessage('note must be > 1 < 2000'),
];

const listInvoice = [
  query('status').isIn(invoiceStatusList).withMessage(`value must be in ${invoiceStatusList}`).optional(),
  offsetAndLimit.offset,
  offsetAndLimit.limit,
];

const updateInvoiceStatus = [
  param('id').isInt().withMessage('id is not valid number'),
  body('status').isIn(invoiceStatusList).withMessage(`value must be in ${invoiceStatusList}`).exists().withMessage('status is required')
];

export default {
  createInvoice,
  listInvoice,
  updateInvoiceStatus,
};
