import NodeCache from 'node-cache';
import ExpressReq from '../../types/expressRequest';
import InvoiceStatus from '../../types/invoice/InvoiceStatus';
import db from '../../models';
import logger from '../../framework/logger';

const invoiceStatus = new NodeCache();

const listInvoiceStatus = async (req: ExpressReq): Promise<InvoiceStatus[]> => {
  try {
    let value: InvoiceStatus[] = invoiceStatus.get('all');
    if (!value) {
      value = await db.InvoiceStatus.findAll({raw: true});
      invoiceStatus.set('all', value);
    }
    return value;
  } catch (e) {
    logger.error(`${req.reqUUID} ${e.message}`);
    throw e;
  }
};

const getStatusByName = async (req: ExpressReq, name: string): Promise<InvoiceStatus> => {
  try {
    let value: InvoiceStatus = invoiceStatus.get(name);
    if (!value) {
      value = await db.InvoiceStatus.findOne({
        where: {name, obsoleted: false},
        raw: true,
      });
      invoiceStatus.set(name, value);
    }
    return value;
  } catch (e) {
    logger.error(`${req.reqUUID} ${e.message}`);
    throw e;
  }
};

const getStatusById = async (req: ExpressReq, id: number): Promise<InvoiceStatus> => {
  try {
    let value: InvoiceStatus = invoiceStatus.get(id);
    if (!value) {
      value = await db.InvoiceStatus.findByPk({
        where: {id, obsoleted: false},
        raw: true,
      });
      invoiceStatus.set(id, value);
    }
    return value;
  } catch (e) {
    logger.error(`${req.reqUUID} ${e.message}`);
    throw e;
  }
};

export default {
  getStatusById,
  getStatusByName,
  listInvoiceStatus
};
