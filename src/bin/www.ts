#!/usr/bin/env node

import app from '../app';
import http from 'http';
import config from '../config';
import logger from '../framework/logger';

// Check required env vars
(() => {
  const varNamesList = [
    'DATA_BASE_NAME',
    'DATA_BASE_USERNAME',
    'DATA_BASE_PASSWORD',
    'DATA_BASE_HOST',
    'DATA_BASE_DIALECT',
  ];
  for (const varNamesListKey of varNamesList) {
    if (!process.env[varNamesListKey]) {
      logger.error(`MISSING REQUIRED ENV VAR => (${varNamesListKey})`);
      process.exit(1);
    }
  }
})();

/**
 * Get port from environment and store in Express.
 */
const port = normalizePort(config.app.port);
app.set('port', port);

/**
 * Create HTTP server.
 */
const server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */
server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

/**
 * Normalize a port into a number, string, or false.
 */
function normalizePort(val: string) {
  const normalizedPort = parseInt(val, 10);

  if (isNaN(normalizedPort)) {
    // named pipe
    return val;
  }

  if (normalizedPort >= 0) {
    // port number
    return normalizedPort;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 */
function onError(error: any) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  const bind = typeof port === 'string' ?
    'Pipe ' + port :
    'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      logger.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      logger.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */
function onListening() {
  const addr = server.address();
  const bind = typeof addr === 'string' ?
    'pipe ' + addr :
    'port ' + addr.port;
  logger.debug('Listening on ' + bind);
}
