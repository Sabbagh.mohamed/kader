import * as dbConfig from './database';

export default {
  app: {
    port: process.env.PORT || '3000',
    env: process.env.NODE_ENV || 'local',
    logLevel: process.env.LOG_LEVEL || 'debug',
  },
  db: {
    ...dbConfig,
  },
};
