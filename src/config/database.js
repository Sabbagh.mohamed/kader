module.exports = {
  username: process.env.DATA_BASE_USERNAME,
  password: process.env.DATA_BASE_PASSWORD,
  database: process.env.DATA_BASE_NAME,
  host: process.env.DATA_BASE_HOST,
  dialect: process.env.DATA_BASE_DIALECT,
};
