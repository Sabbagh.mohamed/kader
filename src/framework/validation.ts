import {validationResult} from 'express-validator';

import express from 'express';

import UnprocessableEntity from './response/error/errors/UnprocessableEntity';
import InternalServerError from './response/error/errors/InternalServerError';
import logger from './logger';
import ExpressReq from '../types/expressRequest';

const validateRequest = async (req: ExpressReq, res: express.Response, next: express.NextFunction) => {
  try {
    const results = await validationResult(req);
    if (results.isEmpty()) {
      return next();
    }
    return next(new UnprocessableEntity(results.array()));
  } catch (e) {
    logger.error(`${req.reqUUID} ${e.message}`);
    return next(new InternalServerError(req));
  }
};

export default validateRequest;
