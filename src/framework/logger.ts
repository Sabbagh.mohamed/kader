import winston from 'winston';
import config from '../config';

const logger = winston.createLogger({
  level: config.app.logLevel,
  transports: [
    new winston.transports.Console({
      format: winston.format.simple(),
    }),
  ],
});

export default logger;
