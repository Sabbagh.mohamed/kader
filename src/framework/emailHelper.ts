import nodemailer from 'nodemailer';
import logger from './logger';

let transporter: nodemailer.Transporter;
let testAccount: nodemailer.TestAccount;
const nodemailerInit = async () => {
  try {
    testAccount = await nodemailer.createTestAccount();
    transporter = await nodemailer.createTransport({
      host: 'smtp.ethereal.email',
      port: 587,
      secure: false,
      auth: {
        user: testAccount.user,
        pass: testAccount.pass,
      },
    });
  } catch (e) {
    logger.error(e.message);
    process.exit(1);
  }
};

const sendEmail = async (to: string, subject: string, body: string, reqUUID: string) => {
  try {
    const emailResponse = await transporter.sendMail({
      from: testAccount.user,
      to,
      subject,
      text: body,
    });
    logger.info(`Message sent to ${to} ${emailResponse.messageId} request UUID ${reqUUID}, Preview URL ${nodemailer.getTestMessageUrl(emailResponse)}`);
  } catch (e) {
    logger.error(`${e.message} ${reqUUID}`);
  }
};

export default {
  nodemailerInit,
  sendEmail,
};
