import httpCodes from '../../../../config/httpCodes';
import GeneralError from '../generalError';

class BadRequest extends GeneralError {
  constructor(message: string, data?: any) {
    super(message, httpCodes.BAD_REQUEST, data);
  }
}

export default BadRequest;
