import httpCodes from '../../../../config/httpCodes';
import GeneralError from '../generalError';

class InternalServerError extends GeneralError {
  constructor(data?: any) {
    super('Internal Server Error', httpCodes.INTERNAL_SERVER_ERROR, data);
  }
}

export default InternalServerError;
