import GeneralError from '../generalError';
import httpCodes from '../../../../config/httpCodes';
import {ValidationError} from 'express-validator';

class UnprocessableEntity extends GeneralError {
  constructor(message: ValidationError[], data?: any) {
    super(message, httpCodes.UNPROCESSABLE_ENTITY, data);
  }
}

export default UnprocessableEntity;
