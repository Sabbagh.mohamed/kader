import express from 'express';
import GeneralError from './generalError';
import ExpressReq from '../../../types/expressRequest';

const handleError = (err: GeneralError, req: ExpressReq, res: express.Response) => {
  const statusCode = err.getStatusCode();
  const message = err.getMessage();
  const data = err.getData();
  return res.status(statusCode).json({
    status: 'failed',
    data,
    message,
    statusCode,
    uuid: req.reqUUID,
  });
};

export default handleError;
