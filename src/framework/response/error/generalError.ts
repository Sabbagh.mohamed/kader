import httpCodes from '../../../config/httpCodes';
import {ValidationError} from 'express-validator';

class GeneralError extends Error {
  private readonly statusCode: httpCodes;
  private readonly data: any;
  readonly message: any;

  constructor(message: string | ValidationError[], statusCode: httpCodes, data: any) {
    super();
    this.statusCode = statusCode;
    this.message = message;
    this.data = data;
  }

  getStatusCode() {
    return this.statusCode;
  }

  getMessage() {
    return this.message;
  }

  getData() {
    return this.data;
  }
}

export default GeneralError;
