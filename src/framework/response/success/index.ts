import GeneralSuccess from './generalSuccess';
import express from 'express';
import ExpressReq from '../../../types/expressRequest';

const handleSuccess = (success: GeneralSuccess, req: ExpressReq, res: express.Response) => {
  const statusCode = success.getStatusCode();
  const message = success.getMessage();
  const data = success.getData();

  return res.status(statusCode).json({
    status: 'success',
    data,
    message,
    dataLength: data?.length > 0 ? data.length : undefined,
    statusCode,
    uuid: req.reqUUID,
  });
};

export default handleSuccess;
