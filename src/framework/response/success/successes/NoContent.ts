import httpCodes from '../../../../config/httpCodes';
import GeneralSuccess from '../generalSuccess';

class NoContent extends GeneralSuccess {
  constructor(message: string, data?:any) {
    super(message, httpCodes.NO_CONTENT, data);
  }
}

export default NoContent;
