import httpCodes from '../../../../config/httpCodes';
import GeneralSuccess from '../generalSuccess';

class Success extends GeneralSuccess {
  constructor(message: string, data?:any) {
    super(message, httpCodes.OK, data);
  }
}

export default Success;
