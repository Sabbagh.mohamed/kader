import httpCodes from '../../../../config/httpCodes';
import GeneralSuccess from '../generalSuccess';

class Created extends GeneralSuccess {
  constructor(message:string, data?:any) {
    super(message, httpCodes.CREATED, data);
  }
}

export default Created;
