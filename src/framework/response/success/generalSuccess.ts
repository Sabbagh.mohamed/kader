import httpCodes from '../../../config/httpCodes';

class GeneralSuccess {
  private readonly statusCode: httpCodes;
  private readonly data: any;
  private readonly message: string;

  constructor(message: string, statusCode: httpCodes, data?: any) {
    this.statusCode = statusCode;
    this.data = data;
    this.message = message;
  }

  getStatusCode() {
    return this.statusCode;
  }

  getMessage() {
    return this.message;
  }

  getData() {
    return this.data;
  }
}

export default GeneralSuccess;
