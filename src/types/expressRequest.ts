import express from 'express';

interface ExpressReq extends express.Request {
  reqUUID: string,
}

export default ExpressReq;
