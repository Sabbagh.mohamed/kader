interface InvoiceItems {
  id: number;
  name: string;
  description: string;
  hours: number;
  rate: number;
  expenses: number;
  created_at: number;
  updated_at: number;
}

export default InvoiceItems;
