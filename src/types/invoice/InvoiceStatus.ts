interface InvoiceStatus {
  id: number;
  name: string;
  created_at: Date;
}

export default InvoiceStatus;
