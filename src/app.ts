import express from 'express';
import compression from 'compression';
import cookieParser from 'cookie-parser';
import logger from 'morgan';
import {v1 as uuidV1} from 'uuid';

import router from './routes';
import GeneralSuccess from './framework/response/success/generalSuccess';
import GeneralError from './framework/response/error/generalError';
import handleError from './framework/response/error';
import handleSuccess from './framework/response/success';
import ExpressReq from './types/expressRequest';
import emailHelper from './framework/emailHelper';
import startCron from './cron';
import queue from './queue';
import invoiceEmailQueue from './queue/invoice/invoiceEmail';

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(compression());
app.use((req: ExpressReq, res, next) => {
  req.reqUUID = uuidV1();
  next();
});

app.disable('x-powered-by');
emailHelper.nodemailerInit().then(()=>{
  startCron();
  queue.invoiceEmailQueue.processEmails();
});

app.use('/api', router);

app.use((service: GeneralSuccess | GeneralError, req: ExpressReq, res: express.Response, next: express.NextFunction) => {
  if (service instanceof GeneralError) {
    return handleError(service, req, res);
  }
  return handleSuccess(service, req, res);
});

export default app;
