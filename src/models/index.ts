import {Sequelize} from 'sequelize';
import fs from 'fs';
import path from 'path';
import config from '../config';

// @ts-ignore
const sequelize: any = new Sequelize(config.db);

const db: any = {};

fs.readdirSync('./src/models')
  .forEach((file) => {
    if (file !== 'index.ts') {
      const model = require(path.join(__dirname, file.slice(0, file.length - 3)))(sequelize, Sequelize);
      db[model.name] = model;
    }
  });

Object.keys(db).forEach((modelName) => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;
db.sequelize.sync({
  logging: false,
});

export default db;
