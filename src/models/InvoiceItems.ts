module.exports = (sequelize: any, DataTypes: any) => {
  const InvoiceItems = sequelize.define(
    'InvoiceItems', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.BIGINT.UNSIGNED,
      },
      invoice_id: {
        type: DataTypes.BIGINT.UNSIGNED,
        allowNull: false,
        references: {
          model: 'invoice',
          key: 'id',
        },
      },
      description: {
        type: DataTypes.STRING(2000),
        allowNull: false,
      },
      hours: {
        type: DataTypes.FLOAT,
        allowNull: false,
      },
      rate: {
        type: DataTypes.FLOAT,
        allowNull: false,
      },
      expenses: {
        type: DataTypes.FLOAT,
        allowNull: false,
      },
      obsoleted: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      created_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.literal('NOW()'),
        allowNull: false,
      },
      updated_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.literal('NOW()'),
        allowNull: false,
      },
    }, {
      tableName: 'invoice_items',
      modelName: 'InvoiceItems',
      createdAt: false,
      updatedAt: false,
    },
  );

  InvoiceItems.associate = (models: any) => {
    models.InvoiceItems.belongsTo(models.Invoice, {
      foreignKey: 'invoice_id',
    });
  };
  return InvoiceItems;
};
