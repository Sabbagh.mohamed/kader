module.exports = (sequelize: any, DataTypes: any) => {
  const InvoiceStatus = sequelize.define(
    'InvoiceStatus', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.BIGINT.UNSIGNED,
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      obsoleted: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      created_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.literal('NOW()'),
        allowNull: false,
      },
    }, {
      tableName: 'invoice_status',
      modelName: 'InvoiceStatus',
      createdAt: false,
      updatedAt: false,
    },
  );

  InvoiceStatus.associate = (models: any) => {
    models.InvoiceStatus.hasMany(models.Invoice, {
      foreignKey: 'status_id',
    });
  };
  return InvoiceStatus;
};
