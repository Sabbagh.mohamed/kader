module.exports = (sequelize: any, DataTypes: any) => {
  const Invoice = sequelize.define(
    'Invoice', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.BIGINT.UNSIGNED,
      },
      status_id: {
        type: DataTypes.BIGINT.UNSIGNED,
        allowNull: false,
        references: {
          model: 'invoice_status',
          key: 'id',
        },
      },
      note: {
        type: DataTypes.STRING(2000),
        allowNull: false,
      },
      invoice_to: {
        type: DataTypes.STRING(100),
        allowNull: false,
      },
      obsoleted: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      created_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.literal('NOW()'),
        allowNull: false,
      },
      updated_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.literal('NOW()'),
        allowNull: false,
      },
    }, {
      tableName: 'invoice',
      modelName: 'Invoice',
      createdAt: false,
      updatedAt: false,
    },
  );

  Invoice.associate = (models: any) => {
    models.Invoice.belongsTo(models.InvoiceStatus, {
      foreignKey: 'status_id',
    });
    models.Invoice.hasMany(models.InvoiceItems, {
      foreignKey: 'invoice_id',
    });
  };
  return Invoice;
};
