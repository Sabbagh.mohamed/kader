import {CronJob} from 'cron';
import {v1 as uuidV1} from 'uuid';
import cache from '../../cache';
import db from '../../models';
import queue from '../../queue';
import logger from '../../framework/logger';

const cronJob = new CronJob('00 00 00 * * *', async () => {

  const {id: paidStatusId} = await cache.invoiceStatus.getStatusByName(null, 'paid');

  const invoices = await db.Invoice.findAll({
    where: {
      status_id: {
        [db.Sequelize.Op.not]: paidStatusId,
      },
    },
    attributes: ['id', 'note', 'invoice_to', 'created_at', 'updated_at'],
    include: [
      {
        model: db.InvoiceStatus,
        where: {
          obsoleted: false,
        },
        required: true,
        attributes: ['name'],
      },
      {
        model: db.InvoiceItems,
        where: {
          obsoleted: false,
        },
        required: true,
        attributes: ['id', 'description', 'hours', 'rate', 'expenses', 'created_at', 'updated_at'],
      },
    ],
    limit: 1,
  });

  const jobs = invoices.map((invoice: any) => {
    return queue.invoiceEmailQueue.invoiceEmail.createJob({
      id: invoice.id, body: {
        items: invoice.InvoiceItems,
        note: invoice.note,
        to: invoice.invoice_to,
      }, requestUUID: uuidV1(),
    })
      .timeout(60000) // 1 min
      .retries(2);
  });
  // https://github.com/bee-queue/bee-queue/issues/318
  // @ts-ignore
  queue.invoiceEmailQueue.invoiceEmail.saveAll(jobs).then((error) => {
    logger.error(error.message);
  });
}, null, true, 'America/Los_Angeles');

export default cronJob;
