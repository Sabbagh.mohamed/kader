import invoiceReminderCron from './invoiceReminder';

const startCron = () => {
  invoiceReminderCron.start();
};

export default startCron;
