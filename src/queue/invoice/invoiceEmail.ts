import Queue from 'bee-queue';
import logger from '../../framework/logger';
import emailHelper from '../../framework/emailHelper';

const invoiceEmail = new Queue('invoiceEmail');

const processEmails = () => {
  // @ts-ignore
  invoiceEmail.process((job, done) => {
    logger.info(`Processing job ${job.id}`);
    const invoiceBody = `Hello Dear,
    Please find the bellow details for your invoice
    ${JSON.stringify(job.data.body)}`;
    emailHelper.sendEmail(job.data.body.to, 'invoice details', invoiceBody, job.data.requestUUID);
    return done(null);
  });
};

export default {
  invoiceEmail,
  processEmails,
};
