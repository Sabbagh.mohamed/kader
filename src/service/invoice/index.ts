import db from '../../models';
import logger from '../../framework/logger';
import ExpressReq from '../../types/expressRequest';
import InvoiceItems from '../../types/invoice/InvoiceItems';
import cache from '../../cache';
import queue from '../../queue';

const createInvoice = async (req: ExpressReq) => {
  try {
    const {id: openStatusId} = await cache.invoiceStatus.getStatusByName(req, 'open');
    const {note, to, items} = req.body;

    const t = await db.sequelize.transaction();
    const {id} = await db.Invoice.create({
      status_id: openStatusId,
      note,
      invoice_to: to,
    }, {transaction: t, returning: true});

    await db.InvoiceItems.bulkCreate(items.map((item: InvoiceItems) => {
      return {...item, invoice_id: id};
    }), {transaction: t, returning: true});

    const job = queue.invoiceEmailQueue.invoiceEmail.createJob({id, body: req.body, requestUUID: req.reqUUID});
    job
      .timeout(60000) // 1 min
      .retries(2)
      .save()
      .then((jobResult) => {
        logger.info(`invoiceEmailJob created with id ${jobResult.id} for request ${req.reqUUID}`);
      });

    await t.commit();

    return {code: 0, message: 'Invoice created', data: {id, ...req.body}};
  } catch (e) {
    logger.error(`${req.reqUUID} ${e.message}`);
    throw e;
  }
};

const listInvoice = async (req: ExpressReq) => {
  try {
    const {status, offset = 0, limit = 100} = req.query;

    const invoiceWhere = {
      obsoleted: false,
      status_id: 0,
    };
    if (status) {
      const {id} = await cache.invoiceStatus.getStatusByName(req, `${status}`);
      invoiceWhere.status_id = id;
    } else {
      delete invoiceWhere.status_id;
    }

    const invoices = await db.Invoice.findAll({
      where: invoiceWhere,
      attributes: ['id', 'note', 'invoice_to', 'created_at', 'updated_at'],
      include: [
        {
          model: db.InvoiceStatus,
          where: {
            obsoleted: false,
          },
          required: true,
          attributes: ['name'],
        },
        {
          model: db.InvoiceItems,
          where: {
            obsoleted: false,
          },
          required: true,
          attributes: ['id', 'description', 'hours', 'rate', 'expenses', 'created_at', 'updated_at'],
        },
      ],
      limit,
      offset,
    });
    if (invoices.length > 0) {
      return {code: 0, data: invoices};
    } else {
      return {
        code: 1,
        message: 'No Data with requested params',
        data: [invoices],
      };
    }
  } catch (e) {
    logger.error(`${req.reqUUID} ${e.message}`);
    throw e;
  }
};

const updateInvoiceStatus = async (req: ExpressReq) => {
  try {
    const {status} = req.body;
    const {id: invoiceId} = req.params;
    const {id: statusId} = await cache.invoiceStatus.getStatusByName(req, `${status}`);

    const [invoice] = await db.Invoice.update(
      {status_id: statusId, updated_at: db.sequelize.fn('NOW')},
      {where: {id: invoiceId}},
    );
    if (invoice === 1) {
      return {code: 0, message: 'invoice status updated'};
    } else {
      return {
        code: 1,
        message: 'Failed to update invoice status, please check the id',
      };
    }
  } catch (e) {
    logger.error(`${req.reqUUID} ${e.message}`);
    throw e;
  }
};

export default {
  createInvoice,
  listInvoice,
  updateInvoiceStatus,
};
