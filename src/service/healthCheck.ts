import db from '../models';
import logger from '../framework/logger';
import ExpressReq from '../types/expressRequest';

const dbHealthCheck = async (req: ExpressReq) => {
  try {
    await db.sequelize.authenticate();
    return {code: 0, message: 'System Operational'};
  } catch (e) {
    logger.error(`${req.reqUUID} ${e.message}`);
    throw e;
  }
};

export default {dbHealthCheck};
