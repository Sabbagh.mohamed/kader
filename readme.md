# Kader Backend Task

### Requirments
- Mysql v8
- Node.js v14
- redis v6
- sequelize cli v6

## How to run
- create kader schema in mysql
- prepare env vars list [PORT, NODE_ENV, LOG_LEVEL, DATA_BASE_NAM, DATA_BASE_USERNAME, DATA_BASE_PASSWORD, DATA_BASE_HOST, DATA_BASE_DIALECT] some has defaulf values
  -- these env are also reqired for all run scripts (start, db:migrate, db:seed)
- npm run db:migrate
- npm run db:seed
- npm i
- npm start

API docs are in `Kader.postman_collection.json` file
